Reproduction steps:

``` bash
rustup override set nightly
rustup run nightly cargo build --verbose --release
mkdir -p target/release/objs
pushd target/release/objs
ar -x ../librust_dup_syms.a
popd
gcc --shared -Wall -o target/librust_dup_syms.so target/release/objs/*.o
```
